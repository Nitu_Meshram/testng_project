# TestNG_Project
My TestNg framework consists of following components,
1.Test execution mechanism, 
2.Test scripts, 
3.Testng Annotations, 
4.Common Function, 
5.Variable file, 
6.Libraries and 
7.Reports.

Test execution mechanism: --> In Execution mechanism I am using testng.xml file to execute my test cases without the need of main method, with the help of @Test annotation in test cases I want to execute. Make use of surefire plugin to run the test cases on mvn CLI tool using mvn test command.

Test scripts: --> Next component is test cript, for each API we have multiple test cases and for each test case we have one single test script, which I have created in Test classes pkg.

Testng Annotations: --> Used different TestNG annotations like @BeforeTest, @test, @AfterTest etc. To execute the methods as per the requirement. Also define the ppriority of Test methods using priority attribute with @test annotation.

Common Function: --> Then in common function, I segregated it into two categories API related common function and Utilities related common function.
In API_common_function I have created methods to trigger the API, fetch the responsebody and extract response status code.
Utilities_common_function, contains methods for creating API logs (endpoint, requestbody, responsebody) once the API is executed and saving them into seperate text files. Second utility is craeting a directory if it does not exist and deleting and then creating it if it already exist. Then I have third utility to read data from excel file using apache poi library.

Variable File: --> In variable file I have used excel file to drive data into my test cases and execute same test case multiple times on different sets od data.

Libraries: --> In libraries I have added Rest Assured Jars for triggering the API, extarcting statuscode, extracting responsebody, parsing response body using JsonPath. Having TestNG library for validating the response parameters using assert class. Then also used apache poi libraries to read and write data from excel file.

Reports: -- > At last I have reporting mechanism where I am using Extent report currently. Earlier used Allure report as well but looking in to it's cacheing problem started using Extent report.
 
Sprint wise execution of test cases can also be possible by creating multiple testng.xml with different names.

Created a method with @DataProvider annotation for data provision. Also used another way of data provision i.e Testng_parameterization where provided data using  parameter tag in testng.xml and @Parameters annotation with Test method. But we can provide only one set of data using parameter tag.

Did parallel execution of test methods using parallel attribute in test tag of testng.xml. Serial_execution takes more time to execute so shifted to parrallel execution where multiple tests or methods or classes can be executed parallely along with thread count of execution.
