package TestngParameterization;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Common_Methods_Pkg.Common_methods;
import Endpoints_Pkg.Patch_endpoint;
import Request_repository_Pkg.Patch_request_repository;
import Request_repository_Pkg.Testng_data_provider;
import Utility_Pkg.Manage_api_logs;
import Utility_Pkg.Manage_directory;
import io.restassured.path.json.JsonPath;

public class Patch_tc extends Common_methods {
	static File log_dir;
	static String requestbody;
	static String endpoint;
	static String responsebody;

	@BeforeTest
	public void Test_setUp() throws IOException {

		log_dir = Manage_directory.create_log_directory("Patch_tc_logs");
		endpoint = Patch_endpoint.patch_endpoint_tc1();
	}
@Parameters({"req_param_name","req_param_job"})
	@Test()
	public static void Patch_executor(String Name, String Job) throws IOException

	{
		requestbody = "{\r\n"
				+ "    \"name\": \""+Name+"\",\r\n"
				+ "    \"job\": \""+Job+"\"\r\n"
				+ "}";	
		
		for (int i = 0; i < 5; i++) {
			int statuscode = patch_statuscode(requestbody, endpoint);
			System.out.println(statuscode);
			{
				if (statuscode == 200) {
					responsebody = patch_responsebody(requestbody, endpoint);
					System.out.println(responsebody);
					Patch_tc.patch_validator(requestbody, responsebody);
					break;
				}

				else {
					System.out.println("Retry since expected status code not found");
				}

			}

		}

	}

	public static void patch_validator(String requestbody, String responsebody) {
		JsonPath request = new JsonPath(requestbody);
		String req_name = request.getString("name");
		String req_job = request.getString("job");
		LocalDateTime sysdate = LocalDateTime.now();
		String exp_date = sysdate.toString().substring(0, 10);

		JsonPath response = new JsonPath(responsebody);
		String res_name = response.getString("name");
		String res_job = response.getString("job");
		String res_date = response.getString("updatedAt");
		res_date = res_date.substring(0, 10);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date, exp_date);

	}
	@AfterTest
	public void Test_tearDown() throws IOException
	{
		Manage_api_logs.create_evidence(log_dir, "Patch_tc", endpoint, requestbody, responsebody);
	}

}
