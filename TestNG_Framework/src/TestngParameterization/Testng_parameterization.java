package TestngParameterization;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Common_Methods_Pkg.Common_methods;
import Endpoints_Pkg.Post_endpoint;
import Request_repository_Pkg.Post_request_repository;
import Request_repository_Pkg.Testng_data_provider;
import Utility_Pkg.Manage_api_logs;
import Utility_Pkg.Manage_directory;
import io.restassured.path.json.JsonPath;

public class Testng_parameterization extends Common_methods {
	static File Log_dir;
	static String requestbody;
	static String endpoint;
	static String responsebody;

	@BeforeTest
	public void Test_setUp() throws IOException {
		Log_dir = Manage_directory.create_log_directory("Post_tc_logs");
		endpoint = Post_endpoint.post_endpoint_tc1();
	}

	@Parameters({ "req_param_name", "req_param_job" })
	@Test()
	public void Post_executor(String req_Name, String req_Job) throws IOException {
		requestbody = "{\r\n" + "    \"name\": \"" + req_Name + "\",\r\n" + "    \"job\": \"" + req_Job + "\"\r\n"
				+ "}";
		for (int i = 0; i < 5; i++) {
			int statuscode = post_statuscode(requestbody, endpoint);
			System.out.println(statuscode);
			{
				if (statuscode == 201) {
					responsebody = post_responsebody(requestbody, endpoint);
					System.out.println(responsebody);
					post_validator(requestbody, responsebody);
					break;
				}

				else {
					System.out.println("Retry as expected status code 201 not found");
				}
			}

		}
	}

	public static void post_validator(String requestbody, String responsebody) {

		JsonPath req_json = new JsonPath(requestbody);
		String req_name = req_json.getString("name");
		String req_job = req_json.getString("job");
		LocalDateTime sysdate = LocalDateTime.now();
		String exp_date = sysdate.toString().substring(0, 10);

		JsonPath res_json = new JsonPath(responsebody);
		String res_name = res_json.getString("name");
		String res_job = res_json.getString("job");
		String res_date = res_json.getString("createdAt").substring(0, 10);
		String res_id = res_json.getString("id");

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date, exp_date);
		Assert.assertNotEquals(res_id, 0);
		Assert.assertNotNull(res_id);
	}

	@AfterTest
	public void Test_tearDown() throws IOException {
		Manage_api_logs.create_evidence(Log_dir, "Post_tc", endpoint, requestbody, responsebody);
	}
}
