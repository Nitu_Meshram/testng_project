package testngDatadriven;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common_Methods_Pkg.Common_methods;
import Endpoints_Pkg.Put_endpoint;
import Request_repository_Pkg.Put_request_repository;
import Request_repository_Pkg.Testng_data_provider;
import Utility_Pkg.Manage_api_logs;
import Utility_Pkg.Manage_directory;
import io.restassured.path.json.JsonPath;

public class Put_tc extends Common_methods {
	static File Log_dir;
	static String requestbody;
	static String endpoint;
	static String responsebody;
	
	@BeforeTest
	public void Test_setUp() throws IOException
	{
		Log_dir=Manage_directory.create_log_directory("Put_tc_logs");
		endpoint =Put_endpoint.put_endpoint_tc1();
}
	
@Test(dataProvider="data_provider", dataProviderClass=Testng_data_provider.class)
	public static void Put_executor(String Name, String job) throws IOException {
	requestbody = "{\r\n"
			+ "    \"name\": \""+Name+"\",\r\n"
			+ "    \"job\": \""+job+"\"\r\n"
			+ "}";
				for (int i = 0; i < 5; i++) 				
			{
			int status_code = put_statuscode(requestbody, endpoint);
			System.out.println(status_code);
			{
				if (status_code == 200) {
					responsebody = put_responsebody(requestbody, endpoint);
					System.out.println(responsebody);
					Put_tc.put_validator(requestbody, responsebody);
					break;
				}

				else {
					System.out.println("Retry since expected statuscode not found");
				}
			}
		}
	}

	public static void put_validator(String requestbody, String responsebody) {
		JsonPath request = new JsonPath(requestbody);
		String req_name = request.getString("name");
		String req_job = request.getString("job");
		LocalDateTime sysdate = LocalDateTime.now();
		String exp_date = sysdate.toString().substring(0, 10);

		JsonPath response = new JsonPath(responsebody);
		String res_name = response.getString("name");
		String res_job = response.getString("job");
		String res_date = response.getString("updatedAt");
		res_date = res_date.substring(0, 10);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date, exp_date);

	}
	@AfterTest
	public void Test_tearDown() throws IOException
	{
		Manage_api_logs.create_evidence(Log_dir, "Put_tc", endpoint, requestbody, responsebody);
	}
}
