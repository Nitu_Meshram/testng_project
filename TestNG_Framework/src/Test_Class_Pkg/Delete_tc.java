package Test_Class_Pkg;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common_Methods_Pkg.Common_methods;
import Endpoints_Pkg.Delete_endpoint;
import Endpoints_Pkg.Post_endpoint;
import Request_repository_Pkg.Post_request_repository;
import Utility_Pkg.Manage_api_logs;
import Utility_Pkg.Manage_directory;


public class Delete_tc extends Common_methods {
	static File Log_dir;
	static String endpoint;
	
	
	@BeforeTest
	public void Test_setUp() throws IOException {
		Log_dir = Manage_directory.create_log_directory("Delete_tc_logs");
		endpoint = Delete_endpoint.delete_endpoint();
	}
	@Test(description="Deleting the record")
	public static void executor() {
		
			for (int i = 0; i < 5; i++) {
			int statuscode = delete_statuscode(endpoint);

			if (statuscode == 204) {
				System.out.println(statuscode);
				break;
			} else {
				System.out.println("Expected statuscode not found hence retrying");
			}
		}
	}
	@AfterTest
	public void Test_tearDown() throws IOException {
		Manage_api_logs.create_evidence(Log_dir, "Delete_tc", endpoint, null, null);
	}
}