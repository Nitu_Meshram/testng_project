package Request_repository_Pkg;

import org.testng.annotations.DataProvider;

public class Testng_data_provider {
	@DataProvider()
	public Object[][] data_provider()
	{
		return new Object[][]
				{
			        {"morpheus","leader"},
			        {"siya","director"},
			        {"abhijit","manager"},
			        {"abhishek","tester"},
			        {"maya","creator"},
			        {"nimisha","reality"},
			        {"mishka","mith"}
				};
		
				
	}
}
