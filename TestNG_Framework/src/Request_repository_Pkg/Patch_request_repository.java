package Request_repository_Pkg;

import java.io.IOException;
import java.util.ArrayList;

import Utility_Pkg.Excel_data_extractor;

public class Patch_request_repository {
	public static String patch_request_tc1() throws IOException {
		ArrayList <String> Data=Excel_data_extractor.Excel_data_reader("Test_Data", "Patch_API", "Patch_TC4");
		String name=Data.get(1);
		String job=Data.get(2);
		String requestbody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n" + "}";
		return requestbody;
	}

}
